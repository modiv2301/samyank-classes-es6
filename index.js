let myArr = [1,5,7,6];
myArr.forEach((value, key)=> {
    console.log("key:"+key+"value"+value)
})
for(let value of myArr) {
    console.log("value:"+value)
}
let myObj = {
    "name": "vivek modi",
    "age": "29",
    "gender": "male"
}

console.log(Object.keys(myObj));
Object.keys(myObj).forEach((objKey)=>{
    console.log("key:"+objKey+"value:",myObj[objKey])
})
for(let objKey of Object.keys(myObj)){
    console.log("key:"+objKey+"value:",myObj[objKey])
}